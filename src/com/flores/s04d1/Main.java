package com.flores.s04d1;

import abstraction.Person;
import inheritance.Dog;
import polymorphism.StaticPolymorphism;

public class Main {
    public static void main(String[] args) {
//        Creating a new instance of Car class.
//        className identifier = new className();
        Car firstCar = new Car("RX7", "Mazda", 1978);
        Car secondCar = new Car("Huracan", "Lamborghini", 2014);

        firstCar.drive(); //method call
        secondCar.drive();

        System.out.println("I have a new car, the " + firstCar.getName());

        firstCar.setName("AE86 Trueno");
        firstCar.setBrand("Toyota");

        System.out.println("I swapped my new car to the " + firstCar.getName());

        System.out.println(firstCar.getBrand());

        Dog firstDog = new Dog();

        System.out.println(firstDog.getBreed());

        firstDog.speak();

        Dog secondDog = new Dog("Hermann", "Brown", "German Shepherd");

        System.out.println(secondDog.getBreed());

        secondDog.speak();

        Person firstPerson = new Person();
        firstPerson.sleep();

        StaticPolymorphism newStatic = new StaticPolymorphism();
        newStatic.sum(1, 2);
        System.out.println(newStatic.sum(1, 2));
        System.out.println(newStatic.sum(1, 2, 3));
        System.out.println(newStatic.sum(1.5, 2.5));
    }
}
