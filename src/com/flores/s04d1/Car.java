package com.flores.s04d1;

public class Car {
//            Properties
//        Syntax: accessModifier dataType identifier;
    private String name;
    private String brand;
    private int yearOfMake;

//    Constructors
//    - Is merely a special purpose function.
//    Syntax: accessModifier functionName() {}

//    Empty constructor
    public Car() {}
//    Parameterized constructor
    public Car(String name, String brand, int yearOfMake) {
        this.name = name;
        this.brand = brand;
        this.yearOfMake = yearOfMake;
    }

//    Getter & Setters
//    They are a set of special functions of a class.
//    But they can only retrieve and update values of the properties.
//    Syntax: accessModifier returnDataType functionName(params) {}

//    property => name
    public String getName() {
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public  String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getYearOfMake() {
        return yearOfMake;
    }

    public void setYearOfMake(int yearOfMake) {
        this.yearOfMake = yearOfMake;
    }

//    Methods
//    Functions that represents the class behavior.
//    Syntax: accessModifier returnDataType functionName(params) {}

    public void drive() {
        System.out.println("Driving the " + this.name);
    }
}
