package abstraction;

public class Person implements Actions {
    public void sleep() {
        System.out.println("ZZZ");
    }

    public void run() {
        System.out.println("Running...");
    }
}
