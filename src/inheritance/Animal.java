package inheritance;

public class Animal {
//    Properties
    private String name;
    private String color;

//    Constructor

    public Animal() {}

    public Animal(String name, String color) {
        this.name = name;
        this.color = color;
    }

//    Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String newColor) {
        this.color = newColor;
    }

//    Methods
    public void call() {
        System.out.println("Hi! My name is " + this.name + ". My color is " + this.color + ".");
    }
}
