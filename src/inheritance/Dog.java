package inheritance;

public class Dog extends Animal{
//    Properties
    private String breed;

//    Default (empty )Constructor
    public Dog() {
        super(); //Animal constructor
        this.breed = "Aspin";
    }

//    Parameterized Constructor
    public Dog(String name, String color, String breed) {
        super(name, color); //Animal constructor
        this.breed = breed;
    }

//    Getter and Setters
    public String getBreed() {
        return breed;
    }

    public void setBreed(String newBreed) {
        this.breed = newBreed;
    }

//    Methods
    public void speak() {
        System.out.println("Woof!");
    }
}
